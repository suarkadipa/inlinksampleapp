class MicropostsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy, :edit, :show]

  def create
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      flash[:success] = "Micropost created!"
      redirect_to root_url
    else
      render 'static_pages/home'
    end
  end

  def destroy
    Micropost.find(params[:id]).destroy
    flash[:success] = "Micropost deleted"
    redirect_to current_user
  end
  
  def edit
    @micropost = Micropost.find(params[:id])
  end
  
  def update
    @micropost = Micropost.find(params[:id])
    if @micropost.update_attributes(micropost_params)
      # Handle a successful update.
      flash[:success] = "Micropost updated"
      redirect_to @micropost
    else
      render 'edit'
    end
  end
  
  def show
    @micropost = Micropost.find(params[:id])
  end

  private

    def micropost_params
      params.require(:micropost).permit(:content)
    end
end
